public abstract class Price {

    abstract int getPriceCode();

    abstract double getCharge(int daysRented);

    int getFrquentRenterPoints(int daysRented) {
        return 1;
    }
}
